<!--- tells the browser to not cache the page and allow a refresh --->
<?php
header("Expires: Fri, 01 Jan 1990 00:00:00 GMT");
header("Pragma: no-cache");
header("Cache-Control: private, no-cache, must-revalidate");
header("Vary: *");
?>

<html>
<head>
<title>Clock</title>
<style>

body {
text-align: center;
color: white;
background-color: #1E326F;
}

div.clock {
font-family: "RobotoLight";
font-size: 160px;
}

div.date {
font-family: "RobotoThin";
font-size: 50px;
}

@font-face {
    font-family: 'Roboto';
    src: url('fonts/Roboto-Regular-webfont.eot');
    src: url('fonts/Roboto-Regular-webfont.eot?#iefix') format('embedded-opentype'),
         url('Roboto-Regular-webfont.woff') format('woff'),
         url('Roboto-Regular-webfont.ttf') format('truetype'),
         url('Roboto-Regular-webfont.svg#RobotoRegular') format('svg');
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'RobotoBold';
    src: url('fonts/Roboto-Bold-webfont.eot');
    src: url('fonts/Roboto-Bold-webfont.eot?#iefix') format('embedded-opentype'),
         url('Roboto-Bold-webfont.woff') format('woff'),
         url('Roboto-Bold-webfont.ttf') format('truetype'),
         url('Roboto-Bold-webfont.svg#RobotoBold') format('svg');
    font-weight: bold;
    font-style: normal;
}

@font-face {
    font-family: 'RobotoThin';
    src: url('fonts/Roboto-Thin-webfont.eot');
    src: url('fonts/Roboto-Thin-webfont.eot?#iefix') format('embedded-opentype'),
         url('Roboto-Thin-webfont.woff') format('woff'),
         url('Roboto-Thin-webfont.ttf') format('truetype'),
         url('Roboto-Thin-webfont.svg#RobotoThin') format('svg');
    font-weight: 200;
    font-style: normal;
}

@font-face {
    font-family: 'RobotoLight';
    src: url('fonts/Roboto-Light-webfont.eot');
    src: url('fonts/Roboto-Light-webfont.eot?#iefix') format('embedded-opentype'),
         url('Roboto-Light-webfont.woff') format('woff'),
         url('Roboto-Light-webfont.ttf') format('truetype'),
         url('Roboto-Light-webfont.svg#RobotoLight') format('svg');
    font-weight: 100;
    font-style: normal;
}

</style>

<script language="JavaScript">
<!--
function clock(){
var time = new Date()
var hr = time.getHours()
var min = time.getMinutes()
var sec = time.getSeconds()
var ampm = " PM "
if (hr < 12){
ampm = " AM "
}
if (hr > 12){
hr -= 12
}
if (hr < 10){
hr = " " + hr
}
if (min < 10){
min = "0" + min
}
if (sec < 10){
sec = "0" + sec
}
document.getElementById("clock").firstChild.nodeValue = hr + ":" + min
setTimeout("clock()", 1000)
}
window.onload=clock;
//-->
</script>

</head>
<body>

<div class=clock>
<span id="clock">&nbsp;</span>
</div>

<div class=date>
<?php
date_default_timezone_set('America/Detroit');
echo date("D, M j Y");
?>
</div>

</body>
</html>
