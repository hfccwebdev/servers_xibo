#! /bin/bash
# fget.sh will download the latest forcast condidtions from the google weather api for Dearborn,MI

rm -rf /webdocs/weather/source/Dearborn.xml
cd /webdocs/weather/source/
wget http://api.wunderground.com/api/f4110493026ede3a/forecast/q/MI/Dearborn.xml

# No longer needed unless Google decides to re release thier api
# while grep 'Unsupported' api > /dev/null
# do
#         echo 'Try again...'
#         rm api
#         wget --cache=off --retry-connrefused -O api http://www.google.com/ig/api?weather=48128
# done
