# wget.sh will download the latest xml weather file for the city
# of your choice.

#! /bin/sh
rm -f /webdocs/weather/source/KDET.xml 	# removes any previous versions of the xml files
cd /webdocs/weather/source		# changes to the directory that you would like to save the xml file too (change as needed)
wget http://www.nws.noaa.gov/data/current_obs/KDET.xml	# downloads the xml file from noaa. change the 4 digit city code to your location
