<!---
    current-temp.php pulls weather from a local file and displays it using php.
    Created: Donald Dille, 2012.
--->


<!--- tells the browser to not cache the page and allow a refresh --->
<?php
header("Expires: Fri, 01 Jan 1990 00:00:00 GMT");
header("Pragma: no-cache");
header("Cache-Control: private, no-cache, must-revalidate");
header("Vary: *");
?>

<html>
<head>
<title>weather small</title>

<!--- refreshes the page every 59 seconds. change the time to match your needs --->
<META HTTP-EQUIV=Refresh CONTENT="1799">

<!--- css styling. right now it is just a drop shadow. change to match your needs --->
<style type="text/css">

body {
text-align: center;
font-family: "RobotoThin";
background-color: #1E326F;
}

div {
color: white;
}

div.date {
font-size: 20px;
font-weight: bold;
}

div.city {
font-family: "Roboto";
font-size: 25px;
}

div.temp {
font-size: 140px;
}

div.icon {
margin-top: -40px;
}

div.cond {
font-family: "Roboto";
font-size: 20px;
}

div.wind {
font-size: 14px;
}

table {
text-transform: uppercase;
font-size: 18px;
text-align: center;
color: #FFFFFF;
}

div.high {
color: #F78181
}

div.low {
color: #81BEF7
}

@font-face {
    font-family: 'Roboto';
    src: url('fonts/Roboto-Regular-webfont.eot');
    src: url('fonts/Roboto-Regular-webfont.eot?#iefix') format('embedded-opentype'),
         url('Roboto-Regular-webfont.woff') format('woff'),
         url('Roboto-Regular-webfont.ttf') format('truetype'),
         url('Roboto-Regular-webfont.svg#RobotoRegular') format('svg');
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'RobotoBold';
    src: url('fonts/Roboto-Bold-webfont.eot');
    src: url('fonts/Roboto-Bold-webfont.eot?#iefix') format('embedded-opentype'),
         url('Roboto-Bold-webfont.woff') format('woff'),
         url('Roboto-Bold-webfont.ttf') format('truetype'),
         url('Roboto-Bold-webfont.svg#RobotoBold') format('svg');
    font-weight: bold;
    font-style: normal;
}

@font-face {
    font-family: 'RobotoThin';
    src: url('fonts/Roboto-Thin-webfont.eot');
    src: url('fonts/Roboto-Thin-webfont.eot?#iefix') format('embedded-opentype'),
         url('Roboto-Thin-webfont.woff') format('woff'),
         url('Roboto-Thin-webfont.ttf') format('truetype'),
         url('Roboto-Thin-webfont.svg#RobotoThin') format('svg');
    font-weight: 200;
    font-style: normal;
}

@font-face {
    font-family: 'RobotoLight';
    src: url('fonts/Roboto-Light-webfont.eot');
    src: url('fonts/Roboto-Light-webfont.eot?#iefix') format('embedded-opentype'),
         url('Roboto-Light-webfont.woff') format('woff'),
         url('Roboto-Light-webfont.ttf') format('truetype'),
         url('Roboto-Light-webfont.svg#RobotoLight') format('svg');
    font-weight: 100;
    font-style: normal;
}

</style>

</head>
<body>

<div class=date>
<?php
date_default_timezone_set('America/Detroit');
echo date("l, M j Y");
?>
</div>
<br> 
<div class="city">
Dearborn </br>
</div>
<!--- php code that pulls the xml file from where your cron job places the download. --->
<!--- change KDET.xml to your area (check the noaa site for city codes) --->
<div class="temp">
<?php
$url = 'http://xibo.hfcc/weather/source/KDET.xml';
$xml = simplexml_load_file($url);
echo round($xml->temp_f); 
echo '&#176;';
?>
</div>

<div class="icon">
<?php
$url = 'http://xibo.hfcc/weather/source/KDET.xml';
$xml = simplexml_load_file($url);
$cond = $xml->weather;
if ($cond == 'Partly Cloudy' || $cond == 'A Few Clouds' || $cond == 'A Few Clouds with Haze' || $cond == 'A Few Clouds and Breezy' || $cond == 'Partly Cloudy' || $cond == 'Partly Cloudy with Haze' || $cond == 'Partly Cloudy and Breezy' || $cond == 'Partly Cloudy and Windy' || $cond == 'A Few Clouds and Windy' || $cond == 'Windy' || $cond == 'Breezy' || $cond == 'Fair and Windy') {
        echo '<img src="../xibo/wicon/weather_mostlysunny.png" width="160" height="160" />';
} elseif ($cond == 'Mostly Cloudy' || $cond == 'Mostly Cloudy with Haze' || $cond == 'Mostly Cloudy and Breezy' || $cond == 'Mostly Cloudy and Windy') {
        echo '<img src="../xibo/wicon/weather_mostlycloudy.png" width="160" height="160" />';
} elseif ($cond == 'Fair' || $cond == 'Clear' || $cond == 'Fair with Haze' || $cond == 'Clear with Haze' || $cond == 'Fair and Breezy' || $cond == 'Clear and Breezy') {
        echo '<img src="../xibo/wicon/weather_sunny.png" width="160" height="160" />';
} elseif ($cond == 'Overcast' || $cond == 'Overcast With Haze' || $cond == 'Overcast and Breezy' || $cond == 'Smoke' || $cond == 'Mostly Cloudy and Windy') {
        echo '<img src="../xibo/wicon/cloudy.png" width="160" height="160" />';
} elseif ($cond == 'Fog/Mist' || $cond == 'Fog' || $cond == 'Freezing Fog' || $cond == 'Shallow Fog' || $cond == 'Partial Fog' || $cond == 'Patches of Fog' || $cond == 'Fog in Vicinity' || $cond == 'Freezing Fog in Vicinity' || $cond == 'Shallow Fog in Vicinity' || $cond == 'Showers in Vicinity Fog' || $cond == 'Patches of Fog in Vicinity' || $cond == 'Partial Fog in Vicinity' || $cond == 'Haze' || $cond == 'Heavy Freezing Fog' || $cond == 'Light Freezing Fog' || $cond == 'Low Blowing Sand' || $cond == 'Blowing Sand' || $cond == 'Sand' || $cond == 'Blowing Dust' || $cond == 'Low Drifting Dust' || $cond == 'Dust' || $cond == 'Heavy Dust Storm' || $cond == 'Dust Storm' || $cond == 'Dust/Sand Whirls in Vicinity' || $cond == 'Dust/Sand Whirls' || $cond == 'Sand Storm in Vicinity' || $cond == 'Heavy Sand Storm' || $cond == 'Sand Storm' || $cond == 'Dust Storm in Vicinity') {
        echo '<img src="../xibo/wicon/weather_fog.png" width="160" height="160" />';
} elseif ($cond == 'Rain' || $cond == 'Heavy Rain' || $cond == 'Rain Fog/Mist' || $cond == 'Heavy Rain Fog/Mist' || $cond == 'Rain Fog' || $cond == 'Heavy Rain Fog' || $cond == 'Drizzle Ice Pellets' || $cond == 'Heavy Rain Ice Pellets' || $cond == 'Light Rain Ice Pellets' || $cond == 'Rain Ice Pellets' || $cond == 'Light Ice Pellets Rain' || $cond == 'Ice Pellets Rain' || $cond == 'Heavy Drizzle Ice Pellets' || $cond == 'Light Drizzle Ice Pellets' || $cond == 'Heavy Ice Pellets Drizzle' || $cond == 'Light Ice Pellets Drizzle' || $cond == 'Ice Pellets Drizzle' || $cond == 'Heavy Ice Pellets Rain' || $cond == 'Heavy Rain Showers' || $cond == 'Light Rain and Breezy' || $cond == 'Light Rain Showers' || $cond == 'Rain Showers' || $cond == 'Showers Rain' || $cond == 'Heavy Showers Rain' || $cond == 'Light Showers Rain' || $cond == 'Rain Showers in Vicinity' || $cond == 'Light Rain Showers Fog/Mist' || $cond == 'Rain Showers Fog/Mist' || $cond == 'Showers Rain in Vicinity' || $cond == 'Heavy Drizzle Fog' || $cond == 'Light Drizzle Fog' || $cond == 'Drizzle Fog' || $cond == 'Light Rain Fog' || $cond == 'Heavy Drizzle Fog/Mist' || $cond == 'Light Drizzle Fog/Mist' || $cond == 'Drizzle Fog/Mist' || $cond == 'Light Rain Fog/Mist' || $cond == 'Heavy Drizzle' || $cond == 'Light Drizzle' || $cond == 'Drizzle' || $cond == 'Light Rain' || $cond == 'Heavy Rain Freezing Drizzle' || $cond == 'Light Rain Freezing Drizzle' || $cond == 'Rain Freezing Drizzle' || $cond == 'Heavy Freezing Drizzle Rain' || $cond == 'Light Freezing Drizzle Rain' || $cond == 'Freezing Drizzle Rain' || $cond == 'Heavy Rain Freezing Rain' || $cond == 'Light Rain Freezing Rain' || $cond == 'Rain Freezing Rain' || $cond == 'Heavy Freezing Rain Rain' || $cond == 'Light Freezing Rain Rain' || $cond == 'Freezing Rain Rain' || $cond == 'Showers Rain in Vicinity Fog/Mist' || $cond == 'Showers Rain Fog/Mist' || $cond == 'Heavy Showers Rain Fog/Mist' || $cond == 'Light Showers Rain Fog/Mist' || $cond == 'Rain Showers in Vicinity Fog/Mist' || $cond == 'Heavy Rain Showers Fog/Mist') {
        echo '<img src="../xibo/wicon/weather_rain.png" width="160" height="160" />';
} elseif ($cond == 'Showers in Vicinity' || $cond == 'Showers in Vicinity Fog/Mist' || $cond == 'Showers in Vicinity Fog' || $cond == 'Showers in Vicinity Haze') {
        echo '<img src="../xibo/wicon/weather_cloudyrain.png" width="160" height="160" />';
} elseif ($cond == 'Thunderstorm in Vicinity' || $cond == 'Thunderstorm in Vicinity Fog' || $cond == 'Thunderstorm in Vicinity Haze' || $cond == 'Funnel Cloud' || $cond == 'Funnel Cloud in Vicinity' || $cond == 'Tornado/Water Spout') {
        echo '<img src="../xibo/wicon/weather_chancestorm.png" width="160" height="160" />';
} elseif ($cond == 'Thunderstorm' || $cond == 'Thunderstorm Rain' || $cond == 'Light Thunderstorm Rain' || $cond == 'Heavy Thunderstorm Rain' ||$cond == 'Heavy Thunderstorm Rain Small Hail/Snow Pellets' || $cond == 'Light Thunderstorm Rain Small Hail/Snow Pellets' || $cond == 'Thunderstorm Rain Small Hail/Snow Pellets' || $cond == 'Thunderstorm Small Hail/Snow Pellets' || $cond == 'Thunderstorm Heavy Rain Hail Fog' || $cond == 'Thunderstorm Light Rain Hail Fog' || $cond == 'Thunderstorm Hail Fog' || $cond == 'Thunderstorm Heavy Rain Hail Haze' || $cond == 'Thunderstorm Light Rain Hail Haze' || $cond == 'Thunderstorm Haze in Vicinity Hail' || $cond == 'Thunderstorm in Vicinity Hail Haze' || $cond == 'Thunderstorm in Vicinity Hail' || $cond == 'Thunderstorm Heavy Rain Hail Fog/Mist' || $cond == 'Thunderstorm Light Rain Hail Fog/Mist' || $cond == 'Thunderstorm Rain Hail Fog/Mist' || $cond == 'Thunderstorm Heavy Rain Hail' || $cond == 'Thunderstorm Light Rain Hail' || $cond == 'Heavy Thunderstorm Rain Hail Fog' || $cond == 'Thunderstorm Hail Fog | Light Thunderstorm Rain Hail Fog' || $cond == 'Heavy Thunderstorm Rain Hail Haze' || $cond == 'Light Thunderstorm Rain Hail Haze' || $cond == 'Thunderstorm Showers in Vicinity Hail' || $cond == 'Heavy Thunderstorm Rain Hail Fog/Hail' || $cond == 'Light Thunderstorm Rain Hail Fog/Mist' || $cond == 'Thunderstorm Rain Hail Fog/Mist' || $cond == 'Heavy Thunderstorm Rain Hail' || $cond == 'Light Thunderstorm Rain Hail' || $cond == 'Thunderstorm Hail' || $cond == 'Thunderstorm Heavy Rain Fog' || $cond == 'Thunderstorm Light Rain Fog' || $cond == 'Thunderstorm Fog' || $cond == 'Thunderstorm Heavy Rain Haze' || $cond == 'Thunderstorm Light Rain Haze' || $cond == 'Thunderstorm Haze in Vicinity' || $cond == 'Thunderstorm in Vicinity Haze' || $cond == 'Thunderstorm Showers in Vicinity' || $cond == 'Thunderstorm in Vicinity Fog/Mist' || $cond == 'Thunderstorm Heavy Rain Fog/Mist' || $cond == 'Thunderstorm Light Rain Fog/Mist' || $cond == 'Thunderstorm Rain Fog/Mist' || $cond == 'Thunderstorm Heavy Rain' || $cond == 'Thunderstorm Light Rain' || $cond == 'Heavy Thunderstorm Rain Fog' || $cond == 'Light Thunderstorm Rain Fog' || $cond == 'Thunderstorm Fog' || $cond == 'Heavy Thunderstorm Rain Haze' || $cond == 'Light Thunderstorm Rain Haze' || $cond == 'Thunderstorm Showers in Vicinity' || $cond == 'Heavy Thunderstorm Rain Fog/Mist' || $cond == 'Heavy Thunderstorm Rain Fog and Windy' || $cond == 'Light Thunderstorm Rain Fog/Mist' || $cond == 'Thunderstorm Rain Fog/Mist') {
        echo '<img src="../xibo/wicon/weather_storm.png" width="160" height="160" />';
} else {
        echo '<img src="../xibo/wicon/weather_snow.png" width="160" height="160" />';
}
?>
</div>

<div class="cond">
<?php
$url = 'http://xibo.hfcc/weather/source/KDET.xml';
$xml = simplexml_load_file($url);
echo $xml->weather;
?>
</div>

<div class="wind">
<img src="../xibo/wicon/windy.png" width="20" height="20" />
<?php
$url = 'http://xibo.hfcc/weather/source/KDET.xml';
$xml = simplexml_load_file($url);
echo $xml->wind_dir, ' ', $xml->wind_mph, '<sup>mph</sup>';
?>
</div>

<!--- Forcast --->

<!--- <?php

function getWeather() {

$url = "http://xibo.hfcc/weather/source/Dearborn.xml";
// Downloads weather data based on location
// $xml_str = file_get_contents($requestAddress,0);
// Parses XML 
$xml = simplexml_load_file($url);
// Loops XML
$count = 0;

echo '<table width="100%">';
echo '<tr>';

foreach($xml->forecast->simpleforecast->forecastdays as $item) {

foreach($item->forecastday as $day) {

echo '<td>';

echo $day->date->weekday_short;
echo '<br>';
$cond = $day->conditions;
if ($cond == 'Mostly Cloudy' || $cond == 'Partly Sunny') {
        echo '<img src="../xibo/wicon/weather_mostlycloudy.png" width="35" height="35" />';
} elseif ($cond == 'Sunny' || $cond == 'Clear') {
        echo '<img src="../xibo/wicon/weather_sunny.png" width="35" height="35" />';
} elseif ($cond == 'Partly Cloudy' || $cond == 'Mostly Sunny') {
        echo '<img src="../xibo/wicon/weather_mostlysunny.png" width="35" height="35" />';
} elseif ($cond == 'Cloudy' || $cond == 'Overcast') {
        echo '<img src="../xibo/wicon/cloudy.png" width="35" height="35" />';
} elseif ($cond == 'Dust' || $cond == 'Fog' || $cond == 'Smoke' || $cond == 'Haze') {
        echo '<img src="../xibo/wicon/weather_fog.png" width="35" height="35" />';
} elseif ($cond == 'Lisht Rain' || $cond == 'Mist' || $cond == 'Rain' || $cond == 'Chance Rain' || $cond == 'Freezing Drizzle' || $cond == 'Showers' || $cond == 'Scattered Showers') {
        echo '<img src="../xibo/wicon/weather_rain.png" width="35" height="35" />';
} elseif ($cond == 'Storm' || $cond == 'Thunderstorm' || $cond == 'Chance of Storm' || $cond == 'Scattered Thunderstorms') {
        echo '<img src="../xibo/wicon/weather_storm.png" width="35" height="35" />';
} else {
        echo '<img src="../xibo/wicon/weather_snow.png" width="35" height="35" />';
}
echo '<br>';
echo '<div class="high">';
echo $day->high->fahrenheit, '&#176;', '<br/>';
echo '</div>';
echo '<div class="low">';
echo $day->low->fahrenheit, '&#176;';
echo '</div>';
echo '</td>';
}

}

echo '</tr>';
echo '</table>';
}

getWeather();

?> --->

</body>
</html>

